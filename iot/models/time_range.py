#  Copyright (c) 2022 Sven Varkel.

from datetime import time
from dataclasses import dataclass


@dataclass
class TimeRange:
    """
    This class holds single time range with start and end.
    It also provides method for checking if given moment
    is within range
    """
    start: time
    end: time

    def is_within_range(self, moment: time):
        """
        Checks if given moment is within range
        :param moment:
        :return:
        """
        return self.start <= moment <= self.end
