#  Copyright (c) 2022 Sven Varkel.

import json
import tinytuya
import ipaddress
from iot import logger
from typing import AnyStr, Dict
from iot.config import ENV_PATH, CONFIG
from dataclasses import dataclass, field
from iot.models.time_info import TimeInfo
from iot.models.price_info import PriceInfo


@dataclass
class Device:
    """
    Device represents single Smart Switch
    """
    name: AnyStr
    id: AnyStr
    key: AnyStr
    # is_on: bool = field(default=False)
    ip_address: ipaddress.ip_address = field(default=None)
    config: Dict = field(default_factory=dict)

    @staticmethod
    async def load_devices():
        """
        Loads device configuration, instantiates these and returns as list
        :return:
        """
        _devices_file = ENV_PATH.joinpath("devices.json")
        with open(_devices_file, "r") as f:
            devices = json.load(fp=f)
        out = list()
        for _d in devices:
            device = await Device.get_instance(**_d)
            out.append(device)
        return out

    @classmethod
    async def get_instance(cls, **kwargs):
        cloud_config_path = ENV_PATH.joinpath("tinytuya.json")
        device = cls(**kwargs)
        device.ip_address = ipaddress.ip_address(kwargs.get("ip_address"))
        with open(cloud_config_path, "r") as f:
            device.cloud_config = json.load(f)
        await device.update_state()
        return device

    async def update_state(self):
        """
        Updates device on/off state from TuYa Cloud
        :return:
        """
        try:
            self.client = tinytuya.Cloud(**self.cloud_config)
            result = self.client.getstatus(deviceid=self.id)
            logger.debug(f"Retrieved status from TuYa Cloud: {result}")
            for r in result.get("result"):
                if r.get("code") == "switch_1":
                    self.config["initial"] = r.get("value")
                    # self.is_on = r.get("value")
                    logger.info(f"Device {self.name} ({self.id}) is on? {self.config.get('initial')}")
        except Exception as ex:
            logger.exception(ex)
        return

    async def do_toggle_switch(self):
        """
        Toggles Smart Switch to opposite state
        :return:
        """
        commands = {
            "commands": [{
                "code": "switch_1",
                "value": not self.is_on
            }, {
                "code": "countdown_1",
                "value": 0
            }]
        }
        result = self.client.sendcommand(self.id, commands)
        logger.debug(f"{__name__} command result: {result}")
        return result

    async def do_turn_off(self):
        """
        Turns Smart Switch off
        :return:
        """
        commands = {
            "commands": [{
                "code": "switch_1",
                "value": False
            }, {
                "code": "countdown_1",
                "value": 0
            }]
        }
        result = self.client.sendcommand(self.id, commands)
        logger.debug(f"{__name__} command result: {result}")
        return result

    async def do_turn_on(self):
        """
        Turns Smart Switch on
        :return:
        """
        commands = {
            "commands": [{
                "code": "switch_1",
                "value": True
            }, {
                "code": "countdown_1",
                "value": 0
            }]
        }
        result = self.client.sendcommand(self.id, commands)
        logger.debug(f"{__name__} command result: {result}")
        return result

    async def price_and_time_check_1(self, time_info: TimeInfo, price_info: PriceInfo, *args, **kwargs):
        """
        This method contains main switching logic
        :param time_info:
        :param price_info:
        :param args:
        :param kwargs:
        :return:
        """
        logger.info(f"making decisions based on current price info: {price_info}")
        logger.info(f"making decisions based on current time info: {time_info.get_active_hours()}")
        _is_active_hours = time_info.is_active_hours()
        _price_ratio = round(price_info.short_past_future_price_ratio(), 3)
        _long_term_price_ratio = round(price_info.long_past_future_price_ratio(), 3)
        _critical_price_ratio = CONFIG.CRITICAL_PRICE_RATIO
        if self.state == "on":
            if not _is_active_hours:
                await self.do_turn_off()
                logger.info(f"turned device {self.name} OFF because it's outside active hours");
            else:
                # if price_info.future_6_hours_avg > price_info.past_6_hours_avg:
                if _price_ratio > _critical_price_ratio:
                    await self.do_turn_off()
                    logger.info(
                        f"turned device {self.name} OFF because short term future price > short term past price")
                else:
                    logger.info(
                        f"device {self.name} is ON and it was NOT turned off because price ratio {_price_ratio} is below {_critical_price_ratio}")
        elif self.state == "off":
            # if price_info.future_6_hours_avg < price_info.past_7_days_avg:
            if _long_term_price_ratio <= _critical_price_ratio:
                await self.do_turn_on()
                logger.info(f"turned device {self.name} ON because short term future price < long term past price")
            if _is_active_hours:
                if _price_ratio <= _critical_price_ratio:  # diff less than 10%
                    await self.do_turn_on()
                    logger.info(
                        f"turned device {self.name} ON because it's within active hours and short term future price / short term past price < 10%")
                else:
                    logger.info(
                        f"device {self.name} is OFF and it was NOT turned of because price ratio {_price_ratio} is above {_critical_price_ratio}")
            else:
                logger.info(
                    f"device {self.name} is OFF, the long term price ratio ({_long_term_price_ratio}) is high and it's outside active hours. Doing nothing.")
        return True
