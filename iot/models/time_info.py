#  Copyright (c) 2022 Sven Varkel.
from typing import List
from datetime import datetime, time
from dataclasses import dataclass, field

from iot.models.time_range import TimeRange
from iot.config import CONFIG


@dataclass
class TimeInfo:
    """
    Class for keeping time ranges and checking active hours
    """
    time_ranges: List[TimeRange] = field(default_factory=list)

    def __init__(self, time_ranges: List = list()):
        """
        Init time ranges here
        """
        if not time_ranges:
            self.time_ranges = list()

        active_hours = CONFIG.ACTIVE_HOURS
        for _period in active_hours:
            # parse start and end hours in string format from config, e.g 08:00 -> (8,0)
            _s, _e = (datetime.strptime(_p, "%H:%M") for _p in _period.split("-"))
            _tr = TimeRange(
                start=time(_s.hour, _s.minute),
                end=time(_e.hour, _e.minute)
            )
            self.time_ranges.append(_tr)

        return

    def is_active_hours(self):
        """
        This method checks if current moment in local time is within predefined "active hour" time ranges
        :return:
        """
        moment = datetime.now().time()
        _is_active_hours = False
        for time_range in self.time_ranges:
            _is_active_hours = time_range.is_within_range(moment=moment)
            if _is_active_hours:
                break
        return _is_active_hours

    def get_active_hours(self):
        """
        This method gathers active hour time ranges into simple list and returns
        :return:
        """
        out = list()
        for time_range in self.time_ranges:
            out.append((f"{time_range.start.strftime('%H:%M')}", f"{time_range.end.strftime('%H:%M')}"))
        return out
