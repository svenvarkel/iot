#  Copyright (c) 2022 Sven Varkel.
import json
from dataclasses import dataclass, field, asdict


@dataclass(init=False)
class PriceInfo:
    past_6_hours_avg: float = field()
    past_6_hours_max: float = field()
    past_6_hours_min: float = field()
    past_7_days_avg: float = field()
    past_7_days_max: float = field()
    past_7_days_min: float = field()
    past_30_days_avg: float = field()
    past_30_days_max: float = field()
    past_30_days_min: float = field()
    past_1_year_avg: float = field()
    past_1_year_max: float = field()
    past_1_year_min: float = field()
    future_6_hours_avg: float = field()
    future_6_hours_min: float = field()
    future_6_hours_max: float = field()

    def pretty(self):
        data = asdict(self)
        return json.dumps(data, indent=4)

    def short_past_future_price_ratio(self):
        ratio = self.future_6_hours_avg / self.past_6_hours_avg
        return ratio

    def long_past_future_price_ratio(self):
        ratio = self.future_6_hours_avg / self.past_7_days_avg
        return ratio
