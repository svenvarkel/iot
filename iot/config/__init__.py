#  Copyright (c) 2022 Sven Varkel.
import os
from pathlib import Path
from environs import Env
from dotenv import load_dotenv

ENV_PATH = Path(os.getcwd()).joinpath("config").joinpath("environment")

_dot_env_file = ENV_PATH.joinpath(".env")
if _dot_env_file.exists():
    load_dotenv(dotenv_path=_dot_env_file)

env = Env()


class Config:
    """
    Application config
    """
    DEBUG = False
    TESTING = False
    ACTIVE_HOURS = env.list("ACTIVE_HOURS")
    PRICE_INFO_BASE_URL = env.url("PRICE_INFO_BASE_URL")
    CRITICAL_PRICE_RATIO = env.float("CRITICAL_PRICE_RATIO", 1.1)


CONFIG = Config()
