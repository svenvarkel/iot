#  Copyright (c) 2022 Sven Varkel.

#
# This program downloads NordPool spot (NPS) prices of electrical energy
# and analyses these. Based on analysis results it switches smart circuits on or off.
#
# Author Sven Varkel <sven.varkel@gmail.com>
#
import sys
import json
import click
import asyncio
import pkg_resources
from iot import logger
from pathlib import Path
from typing import AnyStr
from datetime import datetime
from dataclasses import asdict
from iot.analyzer import Analyzer
from iot.switcher import Switcher
from iot.downloader import Downloader

__version__ = pkg_resources.get_distribution("iot").version

YEAR_RANGE = click.IntRange(2022, 2052)
CURRENT_YEAR = datetime.now().year


@click.group(
    help="This program handles TuYa/SmartLife IoT device switching based on energy prices at Nord Pool.\n\n\n" +
         click.style("Author: Sven Varkel <sven.varkel@gmail.com>", fg="red") +
         "\n\n" +
         f"Current version: {__version__}"
)
@click.version_option(version=__version__)
@click.pass_context
def cli(ctx):
    logger.info(f"Running IoT")
    ctx.ensure_object(dict)
    pass


@cli.command(name="download", help="Download latest price info from Elering API.")
@click.option("--output-dir", "-o", help="Directory for downloaded data.", required=True, type=click.STRING)
@click.option("--year", "-y", help="Year to download data for. Defaults to current year.", required=False,
              type=YEAR_RANGE, default=CURRENT_YEAR)
def download_cmd(output_dir: AnyStr, year: int = CURRENT_YEAR):
    _cmd = click.get_current_context().info_name
    logger.info(f"Running command '{_cmd}' with args: {locals().items()}")
    _dir = Path(output_dir).resolve()
    if not _dir.exists():
        _dir.mkdir(mode=0o755, parents=True)
    out = asyncio.run(Downloader.update_price_file(year=year, download_dir=_dir))
    print(f"Downloaded price info and saved into file {out}.")
    return sys.exit(0)


@cli.command(name="analyze", help="Analyzes and outputs price info.")
@click.option("--input-dir", "-i", help="Data directory that contains downloaded CSV files", required=True)
@click.option("--year", "-y", help="Year to analyze data for. Defaults to current year.", required=False,
              type=YEAR_RANGE,
              default=CURRENT_YEAR)
def analyze_cmd(input_dir: AnyStr, year: int = CURRENT_YEAR):
    _cmd = click.get_current_context().info_name
    logger.info(f"Running command '{_cmd}' with args: {locals().items()}")
    _dir = Path(input_dir).resolve()
    out = asyncio.run(Analyzer.get_price_info(data_dir=_dir, year=year))
    _d = asdict(out)
    _formatted = json.dumps(_d, indent=4)
    print(f"Analyzed price info:")
    print(f"{_formatted}")
    return sys.exit(0)


@cli.command(name="switch", help="Switch smart switches.")
@click.option("--input-dir", "-i", help="Data directory that contains downloaded CSV files", required=True)
@click.option("--year", "-y", help="Year to analyze data for. Defaults to current year.", required=False,
              type=YEAR_RANGE,
              default=CURRENT_YEAR)
def switch_cmd(input_dir: AnyStr, year: int = CURRENT_YEAR):
    _cmd = click.get_current_context().info_name
    logger.info(f"Running command '{_cmd}' with args: {locals().items()}")
    _dir = Path(input_dir).resolve()
    price_info = asyncio.run(Analyzer.get_price_info(data_dir=_dir, year=year))

    if price_info:
        asyncio.run(Switcher.do_switching(price_info=price_info))
        print(f"Finished running switch command.")
        return sys.exit(0)

    return sys.exit(1)


@cli.command(name="show", help="Show price and time info.")
@click.option("--input-dir", "-i", help="Data directory that contains downloaded CSV files", required=True)
@click.option("--year", "-y", help="Year to analyze data for. Defaults to current year.", required=False,
              type=YEAR_RANGE,
              default=CURRENT_YEAR)
def show_cmd(input_dir: AnyStr, year: int = CURRENT_YEAR):
    _cmd = click.get_current_context().info_name
    logger.info(f"Running command '{_cmd}' with args: {locals().items()}")
    _dir = Path(input_dir).resolve()
    price_info = asyncio.run(Analyzer.get_price_info(data_dir=_dir, year=year))
    out = asyncio.run(Switcher.show_info(price_info))

    return sys.exit(0)


if __name__ == "__main__":
    cli()
