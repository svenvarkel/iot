#  Copyright (c) 2022 Sven Varkel.
import pytz
import pandas as pd
from pathlib import Path
from datetime import datetime
from iot.util import try_float
from iot.models.price_info import PriceInfo

from dateutil.relativedelta import relativedelta


class Analyzer:
    """
    This class contains methods for analyzing price info.
    It returns an object (data class) with price info
    for previous and upcoming hours.
    This object is then passed to Switcher
    """

    @staticmethod
    async def get_price_info(data_dir: Path, year: int) -> PriceInfo:
        converters = {
            "NPS Eesti": try_float,
        }
        price_file = data_dir.joinpath(f"nps_price_{year}.csv")
        df = pd.read_csv(price_file, sep=";", converters=converters)
        df = df.rename(columns={
            "Ajatempel (UTC)": "timestamp",
            "NPS Eesti": "price",
            "Kuupäev (Eesti aeg)": "datetime_local"
        })
        df["datetime_local"] = pd.to_datetime(df["datetime_local"], format='%d.%m.%Y %H:%M', dayfirst=True, utc=False)
        df["datetime_utc"] = pd.to_datetime(df["timestamp"], unit="s")
        df = df.sort_values("timestamp")

        df = df.filter(["datetime_local", "price"])
        df = df.rename(columns={"datetime_local": "datetime"})

        _tz_info = pytz.timezone("Europe/Tallinn")
        _now = datetime.now(tz=_tz_info).replace(minute=0, second=0, microsecond=0)
        _back_1_hours_dt = _now - relativedelta(hours=1)
        _back_6_hours_dt = _now - relativedelta(hours=6)
        _fwd_1_hours_dt = _now + relativedelta(hours=1)
        _fwd_6_hours_dt = _now + relativedelta(hours=6)
        _back_7_days_dt = _now - relativedelta(days=7)
        _back_30_days_dt = _now - relativedelta(days=30)
        _back_1_year_dt = _now - relativedelta(years=1)

        _back_6_hours_idx = pd.date_range(_back_6_hours_dt, _back_1_hours_dt, freq="H")
        _fwd_6_hours_idx = pd.date_range(_fwd_1_hours_dt, _fwd_6_hours_dt, freq="H")
        _back_7_days_idx = pd.date_range(_back_7_days_dt, _now, freq="H")
        _back_30_days_idx = pd.date_range(_back_30_days_dt, _now, freq="H")
        _back_1_year_idx = pd.date_range(_back_1_year_dt, _now, freq="H")

        df = df.set_index("datetime")
        df = df.tz_localize(tz=_tz_info)
        _df_back_6 = df[_back_6_hours_idx.min():_back_6_hours_idx.max()]
        _agg_back_6 = _df_back_6.aggregate(["min", "max", "mean"])

        _df_fwd_6 = df[_fwd_6_hours_idx.min():_fwd_6_hours_idx.max()]
        _agg_fwd_6 = _df_fwd_6.aggregate(["min", "max", "mean"])

        _df_back_7_days = df[_back_7_days_idx.min():_back_7_days_idx.max()]
        _agg_back_7_days = _df_back_7_days.aggregate(["min", "max", "mean"])

        _df_back_30_days = df[_back_30_days_idx.min():_back_30_days_idx.max()]
        _agg_back_30_days = _df_back_30_days.aggregate(["min", "max", "mean"])

        # FIXME 1 year back does not make sense now because we only load current year data into dataframe!
        _df_back_1_year = df[_back_1_year_idx.min():_back_1_year_idx.max()]
        _agg_back_1_year = _df_back_1_year.aggregate(["min", "max", "mean"])

        out = PriceInfo()
        out.past_6_hours_max = _agg_back_6["price"]["max"]
        out.past_6_hours_min = _agg_back_6["price"]["min"]
        out.past_6_hours_avg = _agg_back_6["price"]["mean"]

        out.future_6_hours_max = _agg_fwd_6["price"]["max"]
        out.future_6_hours_min = _agg_fwd_6["price"]["min"]
        out.future_6_hours_avg = _agg_fwd_6["price"]["mean"]

        out.past_7_days_max = _agg_back_7_days["price"]["max"]
        out.past_7_days_min = _agg_back_7_days["price"]["min"]
        out.past_7_days_avg = _agg_back_7_days["price"]["mean"]

        out.past_30_days_max = _agg_back_30_days["price"]["max"]
        out.past_30_days_min = _agg_back_30_days["price"]["min"]
        out.past_30_days_avg = _agg_back_30_days["price"]["mean"]

        out.past_1_year_max = _agg_back_1_year["price"]["max"]
        out.past_1_year_min = _agg_back_1_year["price"]["min"]
        out.past_1_year_avg = _agg_back_1_year["price"]["mean"]

        return out
