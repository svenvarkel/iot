#  Copyright (c) 2022 Sven Varkel.
import os
import logging
from pathlib import Path
from typing import AnyStr
from logging.config import dictConfig


def get_log_file(name: AnyStr = f"{__name__}.log"):
    _p = Path(os.getcwd())
    log_dir = _p.joinpath("log")
    if not log_dir.exists():
        log_dir.mkdir(mode=0o755)
    log_file = os.path.join(log_dir, name)
    return log_file


dictConfig({
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "json": {
            "()": "pythonjsonlogger.jsonlogger.JsonFormatter",
            "fmt": "%(name)s %(levelname) %(asctime)s %(message)s"
        },
    },
    "handlers": {
        "null": {
            "class": "logging.NullHandler"
        },
        "stream": {
            "level": logging.DEBUG,
            "formatter": "json",
            "class": "logging.StreamHandler"
        },
        "file": {
            "level": logging.DEBUG,
            "class": "logging.handlers.RotatingFileHandler",
            "formatter": "json",
            "filename": get_log_file(name=f"{__name__}.log"),
            "mode": "a",
            "backupCount": 5
        }
    },
    "loggers": {
        __name__: {
            "handlers": ["file"],
            "level": logging.DEBUG,
            "propagate": False
        },
        "transitions": {
            "handlers": ["file"],
            "level": logging.DEBUG,
            "propagate": False
        }
    },
})

logger = logging.getLogger(__name__)
