#
#  Copyright (c) 2022 Sven Varkel.
#

import codecs
from urllib.parse import ParseResult

import aiohttp
import aiofiles
from iot import logger
from pathlib import Path
from random_user_agent.user_agent import UserAgent

from random_user_agent.params import SoftwareName, OperatingSystem

from iot.config import CONFIG


class Downloader:
    """
    Downloader class contains methods for downloading price info from Elering API
    """

    @staticmethod
    async def update_price_file(year: int, download_dir: Path):
        """
        This method downloads most recent CSV file with NPS prices
        from Elering API
        :return:
        """
        software_names = [SoftwareName.CHROME.value, SoftwareName.FIREFOX.value, SoftwareName.SAFARI.value,
                          SoftwareName.EDGE.value]
        operating_systems = [OperatingSystem.WINDOWS.value, OperatingSystem.LINUX.value, OperatingSystem.MACOS.value]

        try:
            user_agent_rotator = UserAgent(software_names=software_names, operating_systems=operating_systems,
                                           limit=100)
            user_agent = user_agent_rotator.get_random_user_agent()
            headers = {
                "User-Agent": user_agent
            }
            _template: ParseResult = CONFIG.PRICE_INFO_BASE_URL
            _url = _template.geturl().format(start_year=year - 1, end_year=year)
            logger.debug(f"downloading from URL {_url}")
            async with aiohttp.ClientSession(headers=headers) as session:
                async with session.get(url=_url) as response:
                    filename = download_dir.joinpath(f"nps_price_{year}.csv")
                    async with aiofiles.open(filename, mode="wb") as f:
                        async for data in response.content.iter_chunked(1024):
                            await f.write(data)

                    await Downloader.convert_file_to_utf8(file_path=filename)

                return filename
        except Exception as ex:
            logger.exception(ex)

    @staticmethod
    async def convert_file_to_utf8(file_path: Path):
        """
        Converts downloaded file that's in ISO-8859-1 encoding, to UTF-8
        :param file_path:
        :return:
        """
        source_encoding = "iso-8859-1"
        target_encoding = "utf-8"
        try:
            target = file_path.parent.joinpath(".tmp.csv")
            BLOCKSIZE = 1024 * 1024
            with codecs.open(file_path.as_posix(), mode="r", encoding=source_encoding) as f_src:
                with codecs.open(target.as_posix(), mode="w", encoding=target_encoding) as f_target:
                    while True:
                        contents = f_src.read(BLOCKSIZE)
                        if not contents:
                            break
                        f_target.write(contents)
            target.rename(file_path)
            return
        except Exception as ex:
            logger.exception(ex)
