#  Copyright (c) 2022 Sven Varkel.
import random

import pytest
from iot.models.price_info import PriceInfo


def random_float():
    random_float = random.uniform(1, 1000)
    return random_float


@pytest.fixture(name="price_info_past_cheaper")
def price_info_past_cheaper():
    model = PriceInfo()
    model.future_6_hours_avg = random.uniform(300, 1000)
    model.past_6_hours_avg = random.uniform(1, 300)
    model.past_7_days_avg = random.uniform(1, 300)
    return model


@pytest.fixture(name="price_info_future_cheaper")
def price_info_future_cheaper():
    model = PriceInfo()
    model.future_6_hours_avg = random.uniform(1, 300)
    model.past_6_hours_avg = random.uniform(300, 1000)
    model.past_7_days_avg = random.uniform(300, 1000)
    return model


@pytest.fixture(name="price_info_future_a_bit_more_expensive")
def price_info_future_a_bit_more_expensive():
    model = PriceInfo()
    model.future_6_hours_avg = random.uniform(300, 350)
    model.past_6_hours_avg = random.uniform(299, 300)
    model.past_7_days_avg = random.uniform(300, 1000)
    return model
