#  Copyright (c) 2021 Stablewood Properties, Inc.


from iot.tests.unit.fixtures.models.price_info import price_info_past_cheaper
from iot.tests.unit.fixtures.models.price_info import price_info_future_cheaper
from iot.tests.unit.fixtures.models.price_info import price_info_future_a_bit_more_expensive

# these assert are here to make fixtures as "being used" so import optimization does not remove these
assert price_info_past_cheaper
assert price_info_future_cheaper
assert price_info_future_a_bit_more_expensive
