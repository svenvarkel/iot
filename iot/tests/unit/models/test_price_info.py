#  Copyright (c) 2022 Sven Varkel.

def test_price_info_future_cheaper(price_info_future_cheaper):
    ratio = price_info_future_cheaper.short_past_future_price_ratio()
    assert ratio


def test_price_info_past_cheaper(price_info_past_cheaper):
    ratio = price_info_past_cheaper.short_past_future_price_ratio()
    assert ratio


def test_price_info_future_a_bit_more_expensive(price_info_future_a_bit_more_expensive):
    ratio = price_info_future_a_bit_more_expensive.short_past_future_price_ratio()
    assert ratio
