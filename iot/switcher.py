#  Copyright (c) 2022 Sven Varkel.
from urllib.parse import ParseResult

from iot import logger
from typing import AnyStr
from datetime import datetime
from iot.config import CONFIG
from iot.models.device import Device
from dataclasses import dataclass, field
from iot.models.time_info import TimeInfo
from iot.models.price_info import PriceInfo
from transitions.extensions import AsyncMachine
from transitions.extensions.factory import MachineFactory


@dataclass
class Rule:
    name: AnyStr = field()


class Model():
    pass


class Switcher:
    """
    Switcher contains switching logic based on time and price
    """

    @staticmethod
    def get_machine(device: Device):
        """
        Creates, configures and returns machine and list of possible triggers from initial state
        :param device:
        :return:
        """
        machine_cls = MachineFactory.get_predefined(asyncio=True)
        transitions = [
            {"trigger": "turn_on", "source": "off", "dest": "on", "conditions": "price_and_time_check_1"},
            {"trigger": "turn_off", "source": "on", "dest": "off", "conditions": "price_and_time_check_1"}
        ]
        states = [
            "on",
            "off"
        ]
        initial = "on" if device.config.get("initial") else "off"

        machine: AsyncMachine = machine_cls(
            name=device.name,
            model=device,
            states=states,
            transitions=transitions,
            initial=initial,
            auto_transitions=False)
        triggers = machine.get_triggers(initial)
        return machine, triggers

    @staticmethod
    async def do_switching(price_info: PriceInfo):
        """
        This method runs switching logic and calls switching method on devices
        :param price_info:
        :return:
        """
        devices = await Device.load_devices()
        time_info = TimeInfo(time_ranges=list())

        for device in devices:
            machine, triggers = Switcher.get_machine(device=device)
            logger.debug(f"machine state: {device.state}")
            # since it's a simple machine on/off we only have 1 possible state
            # here we try to trigger it on the model
            await device.trigger(triggers[0], time_info=time_info, price_info=price_info)

        return

    @staticmethod
    async def show_info(price_info: PriceInfo):
        url:ParseResult = CONFIG.PRICE_INFO_BASE_URL
        time_info = TimeInfo()
        print(f"Current datetime: {datetime.now()}")
        print(f"UTC datetime: {datetime.utcnow()}")
        print(f"Active hours: {time_info.get_active_hours()}")
        print(f"Is within active hours: {time_info.is_active_hours()}")
        print(f"Price info: {price_info.pretty()}")
        print(f"Price info base URL: {url.geturl()}")
        return
