#  Copyright (c) 2022 Sven Varkel.

def try_float(value):
    try:
        if value is not None:
            value = float(value.replace(",", "."))
    except Exception as ex:
        print(ex)
    return value
